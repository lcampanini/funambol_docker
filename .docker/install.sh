#!/bin/bash
################################
# Automatic installation of a Funambol Cared Edition
################################

#global vars
SCRIPT_PATH=`dirname $0`
SCRIPT_PATH=`pwd`
echo "path: $SCRIPT_PATH"

#cared 10.0.3GA - Quattro
DOWNLOAD_LOCATION_100="/home/funambol/products/Funambol/v10/ga/cared/funambol-carrier-edition-10.0.3.tgz"

#cared 10.5.0 - Internal Release
DOWNLOAD_LOCATION_1050="/home/funambol/products/Funambol/v10.5/internal-release/cared/funambol-carrier-edition-10.5.0.tgz"

#cared 11.0.3 - Testarossa
DOWNLOAD_LOCATION_1103="/home/funambol/products/Funambol/v11/internal-release/11.0.3/onemediahub-11.0.3.tgz"

#cared 11.1.0 Testarossa+
DOWNLOAD_LOCATION_1110="/home/funambol/products/Funambol/v11/ga/onemediahub-11.1.0.tgz"

#cared 11.2.1 Vanguard
DOWNLOAD_LOCATION_1121="/home/funambol/products/Funambol/v11.2/v11.2.1/ga/onemediahub-11.2.1.tgz"

#cared 11.2.3
DOWNLOAD_LOCATION_1123="/home/funambol/products/Funambol/v11.2/v11.2.3/ga/onemediahub-11.2.3.tgz"

#cared 11.2.4 GA
DOWNLOAD_LOCATION_1124="/home/funambol/products/Funambol/v11.2/v11.2.4/ga/onemediahub-11.2.4.tgz"

#cared 11.2.4.1-SNAPSHOT
DOWNLOAD_LOCATION_11241="/home/funambol/products/Funambol/v11.2/v11.2.4/developer-release/latest/onemediahub-11.2.4.1-SNAPSHOT.tgz"

#cared 12.0.1 MAVERICK GA
DOWNLOAD_LOCATION_1201="/home/funambol/products/Funambol/v12/v12.0.1/ga/onemediahub-12.0.1.tgz"

#cared 12.1.0 NEBBIOLO
DOWNLOAD_LOCATION_1210="/home/funambol/products/Funambol/v12.1/RC/onemediahub-12.1.0.tgz"

#cared 12.2.0
DOWNLOAD_LOCATION_1221="/home/funambol/products/Funambol/v12.2/GA/onemediahub-12.2.2.tgz"

#cared 13.0.3 LATEST
DOWNLOAD_LOCATION_LATEST="/home/funambol/products/Funambol/v13/developer-release/latest/onemediahub-13.0.3.1-SNAPSHOT.tgz"

#cared 13.5.1-SNAPSHOT STABILIZATION
DOWNLOAD_LOCATION_1351="/home/funambol/products/Funambol/v13.5/GA/onemediahub-13.5.1.tgz"

DOWNLOAD_LOCATION_1400="/home/funambol/products/Funambol/v14/developer-release/latest/onemediahub-14.0.2.1-SNAPSHOT.tgz"

DOWNLOAD_LOCATION_1450="/home/funambol/products/Funambol/v14.5/GA/onemediahub-14.5.0.tgz"

DOWNLOAD_LOCATION_1500="/home/funambol/products/Funambol/v15/developer-release/server/latest/onemediahub-15.0.0.1-SNAPSHOT.tgz"

DOWNLOAD_LOCATION_1550="/home/funambol/products/Funambol/v15.5/developer-release/server/latest/onemediahub-15.5.1.1-SNAPSHOT.tgz"

DOWNLOAD_LOCATION_1600="https://cucinotta.funambol.com/deliveries/index.php?method=getfile&dir=%2Fv16%2Fdeveloper-release%2Fserver%2Flatest&file=onemediahub-16.0.0.1-SNAPSHOT.tgz"

DOWNLOAD_LOCATION_1700="https://cucinotta.funambol.com/deliveries/index.php?method=getfile&dir=%2Fv17%2FGA&file=onemediahub-17.0.0.tgz"

DOWNLOAD_LOCATION_1750="https://cucinotta.funambol.com/deliveries/index.php?method=getfile&dir=%2Fv17.5%2FGA&file=onemediahub-17.5.0.tgz"

DOWNLOAD_LOCATION_1760="https://cucinotta.funambol.com/deliveries/index.php?method=getfile&dir=%2Fv17.6%2Fdeveloper-release%2Fserver%2Flatest&file=onemediahub-17.6.0.1-SNAPSHOT.tgz"

DOWNLOAD_LOCATION_1800="https://cucinotta.funambol.com/deliveries/index.php?method=getfile&dir=%2Fv18%2FGA&file=onemediahub-18.0.0.tgz"

DOWNLOAD_LOCATION_1810="https://cucinotta.funambol.com/deliveries/index.php?method=getfile&dir=%2Fv18.1%2FGA&file=onemediahub-18.1.0.tgz"

DOWNLOAD_LOCATION_1900="https://cucinotta.funambol.com/deliveries/index.php?method=getfile&dir=%2Fv19%2FGA&file=onemediahub-19.0.0.tgz"

DOWNLOAD_LOCATION_1950="https://cucinotta.funambol.com/deliveries/index.php?method=getfile&dir=%2Fv19.5%2Fdeveloper-release%2Fserver%2Flatest&file=onemediahub-19.5.0-SNAPSHOT.tgz"
DOWNLOAD_TEST_LOCATION="https://cucinotta.funambol.com/deliveries/index.php?method=getfile&dir=%2Fv19.5%2Fdeveloper-release%2Fserver%2Flatest&file=funambol-acceptance-test-19.5.1.3-SNAPSHOT.zip"

DOWNLOAD_LOCATION_2000="https://cucinotta.funambol.com/deliveries/index.php?method=getfile&dir=%2Fv20.0%2Fdeveloper-release%2Fserver%2Flatest&file=onemediahub-20.0.0-SNAPSHOT.tgz"

# default configuration
TAR_FILE="tar.tgz"
ZIP_TEST_FILE="zip_test.zip"

# OMH or OBH
echo
echo "Do you want install OMH or OBH?"
echo "(1) OMH"
echo "(2) OBH"

read -n2 -p "  > " PRODUCT
case $PRODUCT in
  1)
	BASE_PATH="/opt/onemediahub"
	DATABASE="onemediahub"
  ;;
  2)
	BASE_PATH="/opt/onebizhub"
	DATABASE="onebizhub"
  ;;
esac

FUNAMBOL_HOME=$BASE_PATH/onemediahub
case $PRODUCT in
  1) 
	#ask for cared version
	echo
	echo "What cared version you need to install?"
	echo " (1) Cared 8.0.14"
	echo " (2) Cared 8.2.7"
	echo " (3) Cared 8.5.4 Diablo GA"
	echo " (4) Cared 8.7.3 Gallardo GA"
	echo " (5) Cared 9.0.0 Cabriolet"
	echo " (6) Cared 10.0.3 Quattro GA"
	echo " (7) Cared 8.2.1 BT"
	echo " (8) Cared 10.5.0 Internal Release"
	echo " (9) Cared 11.0.3 Testarossa"
	echo " (10) Cared 11.1.0 Testarossa+"
	echo " (11) Cared 11.2.1 Vanguard"
	echo " (12) Cared 11.2.3 "
	echo " (13) Cared 11.2.4 "
	echo " (14) Cared 11.2.4.1-SNAPSHOT"
	echo " (15) Cared 12.0.1 Maverick GA"
	echo " (16) Cared 12.1.2 Nebbiolo GA"
	echo " (17) Cared 12.2.2 NO NAME GA"
	echo " (18) Cared 13.0.3 Langhe"
	echo " (19) Cared 13.5.1 Barolo"
	echo " (20) Cared 14.0.2 Chianti"
	echo " (21) Cared 14.5.0 Morellino"
	echo " (22) Cared 15.0.0 Brunello"
	echo " (23) Cared 15.5.1 Prosecco"
	echo " (24) Cared 16.0.0 Soave"
	echo " (25) Cared 17.0.0 Amarone"
	echo " (26) Cared 17.5.0 Barbera"
	echo " (27) Cared 17.6.0 Cannonau"
	echo " (28) Cared 18.0.0 Dolcetto"
	echo " (29) Cared 18.1.0 Etna"
	echo " (30) Cared 19.0.0 Falanghina Latest"
	echo " (31) Cared 19.5.0 Gotturnio Trunk"
	echo " (32) Cared 20.0 Trunk"
	echo " (0) Exit"

	read -n2 -p "  > " CAREDVERSION

	#assign cared-related variables
	case $CAREDVERSION in
	  1)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_8014
	      FUNAMBOL_HOME=$BASE_PATH/Funambol
	  ;;
	  2)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_827
	      FUNAMBOL_HOME=$BASE_PATH/Funambol
	  ;;
	  3)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_852
	      FUNAMBOL_HOME=$BASE_PATH/Funambol
	  ;;
	  4)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_873
	      FUNAMBOL_HOME=$BASE_PATH/Funambol
	  ;;
	  5)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_900
	      FUNAMBOL_HOME=$BASE_PATH/Funambol
	  ;;
	  6)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_100
	      FUNAMBOL_HOME=$BASE_PATH/Funambol
	  ;;
	  7)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_821
	      FUNAMBOL_HOME=$BASE_PATH/Funambol
	  ;;
	  8)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_1050
	      FUNAMBOL_HOME=$BASE_PATH/Funambol
	  ;;
	  9)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_1103
	  ;;
	  10)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_1110
	  ;;
	  11)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_1121
	  ;;
	  12)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_1123
	  ;;
	  13)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_1124
	  ;;
	  14)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_11241
	  ;;
	  15)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_1201
	  ;;
	  16)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_1210
	  ;;
	  17)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_1221
	  ;;
	  18)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_LATEST
	  ;;
	  19)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_1351
	  ;;
	  20)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_1400
	  ;;
	  21)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_1450
	  ;;
	  22)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_1500
	  ;;
	  23)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_1550
	  ;;
	  24)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_1600
	  ;;
	  25)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_1700
	  ;;
	  26)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_1750
	  ;;
	  27)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_1760
	  ;;
	  28)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_1800
	  ;;
	  29)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_1810
	  ;;
	  30)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_1900
	  ;;
	  31)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_1950
	      DOWNLOAD_TEST_LOCATION=$DOWNLOAD_TEST_LOCATION_1950
	  ;;
	  32)
	      DOWNLOAD_LOCATION=$DOWNLOAD_LOCATION_2000
	  ;;
	  * )
	       echo " Abort..."
	       echo
	       exit 1
	  ;;
	esac
;;
  2)
      DOWNLOAD_LOCATION="https://cucinotta.funambol.com/deliveries/index.php?method=getfile&dir=%2Fonebizhub%2Fproduct%2Fv19.5%2Fdeveloper-release%2Fserver%2Flatest&file=onebizhub-19.5.0-SNAPSHOT.tgz"
      DOWNLOAD_TEST_LOCATION="https://cucinotta.funambol.com/deliveries/index.php?method=getfile&dir=%2Fonebizhub%2Fproduct%2Fv19.5%2Fdeveloper-release%2Fserver%2Flatest&file=funambol-acceptance-test-19.5.1-SNAPSHOT.zip"
  ;;
  * )
       echo " Abort..."
       echo
       exit 1
  ;;
esac


#ask and dowload cared package
echo
read -n1 -p "Download cared from cucinotta (Y or N)? " DOWNLOAD_TAR
case $DOWNLOAD_TAR in
  "Y" | "y")
      echo
      echo DOWNLOADING latest cared...
      echo " downloading $DOWNLOAD_LOCATION in $SCRIPT_PATH/."
      wget $DOWNLOAD_LOCATION -O $TAR_FILE
  ;;
esac

echo
read -n1 -p "Execution AT test (Y or N)?" TEST
case $TEST in
    "Y" | "y")
	read -n1 -p "Download cared from cucinotta (Y or N)? " DOWNLOAD_ZIP
	case $DOWNLOAD_ZIP in
	  "Y" | "y")
		echo
		echo DOWNLOADING test...
		echo " downloading $DOWNLOAD_TEST_LOCATION in $SCRIPT_PATH/."
		wget $DOWNLOAD_TEST_LOCATION -O $ZIP_TEST_FILE
		;;

	esac
	TEST="y"
    ;;
esac
echo

echo "Funambol home $FUNAMBOL_HOME"

#rm .env
echo "MYSQL_DATABASE=$DATABASE" >> .env
echo "FUNAMBOL_HOME=$FUNAMBOL_HOME" >> .env
echo "BASE_PATH=$BASE_PATH" >> .env
echo "PRODUCT=$PRODUCT" >> .env
echo "TEST=$TEST" >> .env

tar -x onemediahub/portal/database/mysql/cared-mysql.sql -zf $TAR_FILE -C ./ --strip-components=4
mv ./cared-mysql.sql db/cared-mysql.sql
echo "update User_ set active_ = true where active_ is false;" >> db/cared-mysql.sql
echo "update fnbl_user set active=1 where active=0;" >> ./db/cared-mysql.sql

case $TEST in
   "Y" | "y")
	echo "insert into User_ values
				('papiuser2','liferay.com','2017-08-22 13:37:38','2017-08-22 13:37:38','','72r0fCDCmKaWkWmFYbWkZA==',1,NULL,0,'papiuser2@gmail.com','en_US','GMT','papiuser2','800x600','',NULL,'',NULL,NULL,'',0,0,1,'Y',NULL,NULL,NULL,1503409057710,0),
				('papicsr','liferay.com','2017-08-22 13:37:38','2017-08-22 13:37:38','','3KCW1W61gQ4=',1,NULL,0,'papicsr@gmail.com','en_US','GMT','papicsr','800x600','',NULL,'',NULL,NULL,'',0,0,1,'Y',NULL,NULL,NULL,1503409058231,0),
				('papiadmin1','liferay.com','2017-08-22 13:37:38','2017-08-22 13:37:38','','iTzg+DZHznzeYLrjeQMDUA==',1,NULL,0,'papiadmin1@gmail.com','en_US','GMT','papiadmin1','800x600','','2017-08-22 13:52:22','','2017-08-22 13:38:39',NULL,'',0,0,1,'Y',NULL,NULL,'2017-08-22 13:38:37',1503409058137,0),
				('papiuser1','liferay.com','2017-08-22 13:37:37','2017-08-22 13:37:37','','72r0fCDCmKYIJ418l1IonA==',1,NULL,0,'papiuser1@gmail.com','en_US','GMT','papiuser1','800x600','','2017-08-22 13:40:38','','2017-08-22 13:39:39',NULL,'',0,0,1,'Y','71QaCljd','2017-08-23 13:50:19','2017-08-22 13:38:39',1503409118900,0),
				('papiinspector','liferay.com','2017-08-22 13:37:38','2017-08-22 13:37:38','','0lvB8OEjuyu87LbNhBgJTw==',1,NULL,0,'papiinspector@gmail.com','en_US','GMT','papiinspector','800x600','','2017-08-22 13:37:51','',NULL,NULL,'',0,0,1,'Y',NULL,NULL,'2017-08-22 13:37:54',1503409057875,0),
				('papiadmin2','liferay.com','2017-08-22 13:37:38','2017-08-22 13:37:38','','iTzg+DZHznxyVHEvA1T9qQ==',1,NULL,0,'papiadmin2@gmail.com','en_US','GMT','papiadmin2','800x600','',NULL,'',NULL,NULL,'',0,0,1,'Y',NULL,NULL,NULL,1503409057992,0)
			;

			insert into Contact_ values
				('papiuser2','liferay.com','papiuser2','','2017-08-22 13:37:38','2017-08-22 13:37:38','liferay.com','-1','','','','','','',0,'1900-01-01 13:37:38','','','','','','','','','','','',''),
				('papicsr','liferay.com','papicsr','','2017-08-22 13:37:38','2017-08-22 13:37:38','liferay.com','-1','','','','','','',0,'1900-01-01 13:37:38','','','','','','','','','','','',''),
				('papiadmin1','liferay.com','papiadmin1','','2017-08-22 13:37:38','2017-08-22 13:37:38','liferay.com','-1','','','','','','',0,'1900-01-01 13:37:38','','','','','','','','','','','',''),
				('papiuser1','liferay.com','papiuser1','','2017-08-22 13:37:37','2017-08-22 13:37:37','liferay.com','-1','','','','','','',0,'1900-01-01 13:37:37','','','','','','','','','','','',''),
				('papiinspector','liferay.com','papiinspector','','2017-08-22 13:37:38','2017-08-22 13:37:38','liferay.com','-1','','','','','','',0,'1900-01-01 13:37:38','','','','','','','','','','','',''),
				('papiadmin2','liferay.com','papiadmin2','','2017-08-22 13:37:38','2017-08-22 13:37:38','liferay.com','-1','','','','','','',0,'1900-01-01 13:37:38','','','','','','','','','','','','')
			;

			insert into fnbl_user_role values
				('papiuser2','standard',NULL),
				('papiuser2','sync_user',NULL),
				('papicsr','csr',NULL),
				('papiadmin1','sync_administrator',NULL),
				('papiuser1','standard',NULL),
				('papiuser1','sync_user',NULL),
				('papiinspector','inspector',NULL),
				('papiadmin2','sync_administrator',NULL)
			;" \ >> ./db/cared-mysql.sql

			echo "INSERT INTO fnbl_user_preference VALUES ('papiadmin1', NULL, 'n', '8', 'y', '', 10, NULL, 10, NULL, 'n', NULL, 0, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0);
			INSERT INTO fnbl_file_data_object_user_lock (userid, total_used_storage, total_used_storage_last_update, total_soft_deleted_storage) 
			VALUES ('papiadmin1', 0, 0, 0);
			INSERT INTO fnbl_user_preference VALUES ('papiadmin2', NULL, 'n', '8', 'y', '', 10, NULL, 10, NULL, 'n', NULL, 0, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0);
			INSERT INTO fnbl_file_data_object_user_lock (userid, total_used_storage, total_used_storage_last_update, total_soft_deleted_storage) 
			VALUES ('papiadmin2', 0, 0, 0); 
			INSERT INTO fnbl_user_preference VALUES ('papiuser1', NULL, 'n', '8', 'y', '', 10, NULL, 10, NULL, 'n', NULL, 0, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0);
			INSERT INTO fnbl_file_data_object_user_lock (userid, total_used_storage, total_used_storage_last_update, total_soft_deleted_storage) 
			VALUES ('papiuser1', 0, 0, 0); 
			INSERT INTO fnbl_user_preference VALUES ('papiuser2', NULL, 'n', '8', 'y', '', 10, NULL, 10, NULL, 'n', NULL, 0, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0);
			INSERT INTO fnbl_file_data_object_user_lock (userid, total_used_storage, total_used_storage_last_update, total_soft_deleted_storage) 
			VALUES ('papiuser2', 0, 0, 0); 
			INSERT INTO fnbl_user_preference VALUES ('papicsr', NULL, 'n', '8', 'y', '', 10, NULL, 10, NULL, 'n', NULL, 0, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0);
			INSERT INTO fnbl_file_data_object_user_lock (userid, total_used_storage, total_used_storage_last_update, total_soft_deleted_storage) 
			VALUES ('papicsr', 0, 0, 0); 
			INSERT INTO fnbl_user_preference VALUES ('papiinspector', NULL, 'n', '8', 'y', '', 10, NULL, 10, NULL, 'n', NULL, 0, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0);
			INSERT INTO fnbl_file_data_object_user_lock (userid, total_used_storage, total_used_storage_last_update, total_soft_deleted_storage) 
			VALUES ('papiinspector', 0, 0, 0);" >> ./db/cared-mysql.sql

			echo "insert into fnbl_user_subscription values
				(0, 'papiuser1','trial',1,1502467039011,1502467039011,'2117-08-11 15:57:19','2017-08-11 15:57:19',0,0,NULL,'2117-08-08 15:57:19','2017-08-11 15:57:19',NULL,NULL,NULL,NULL,NULL)
			;
			insert into fnbl_user_subscription values
				(1, 'papiuser2','trial',1,1502467039011,1502467039011,'2117-08-11 15:57:19','2017-08-11 15:57:19',0,0,NULL,'2117-08-08 15:57:19','2017-08-11 15:57:19',NULL,NULL,NULL,NULL,NULL)
			;" >> ./db/cared-mysql.sql

			echo "create table fnbl_sms (
				id		bigint not null primary key,
				time		bigint not null,
				sender		varchar(255),
				recipient	varchar(255),
				format		varchar(255),
				udh		varchar(255),
				body		varchar(512)
			);" >> ./db/cared-mysql.sql

			echo "create table fnbl_email (
				id		bigint not null primary key,
				time		bigint not null,
				sender		varchar(255),
				recipients 	varchar(255),
				title 		text,
				body 		text
			);" >> ./db/cared-mysql.sql
    ;;
esac


tar -x onemediahub/bin/config.properties -zf $TAR_FILE -C ./ --strip-components=2
mv ./config.properties jdk/config.properties

cp ./tar.tgz jdk/
case $TEST in
  "Y"| "y")
	mv ./zip_test.zip jdk/
  ;;
esac

docker-compose build
docker-compose up -d
#docker-compose --verbose up -d
#docker stop jdk
#sleep 2m
#docker start jdk
