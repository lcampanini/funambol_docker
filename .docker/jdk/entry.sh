#!/bin/bash
SCRIPT_PATH=`dirname $0`
SCRIPT_PATH=`pwd`

CONFIG_FILE_ORI=/tmp/config.properties
CONFIG_FILE=$FUNAMBOL_HOME/bin/config.properties

IPADDRESS=$SERVER_IPADDRESS:8080

echo
echo
echo DIRECTORY $BASE_PATH cleaning and creation...
rm -rf $BASE_PATH
mkdir -p $BASE_PATH
echo FILES from $TAR_FILE into $BASE_PATH extraction...
tar -C $BASE_PATH -xf $TAR_FILE

echo
echo "CONFIG FILE copying and values replacing..."
cp $CONFIG_FILE_ORI $CONFIG_FILE
echo "\${portal.server}=$IPADDRESS" >> $CONFIG_FILE
##for portal before v11
echo "\${media.server.url}=$IPADDRESS" >> $CONFIG_FILE
echo "\${media.server}=$IPADDRESS" >> $CONFIG_FILE
echo "\${jdbc.user}=$database_username" >> $CONFIG_FILE
echo "\${jdbc.password}=$database_password" >> $CONFIG_FILE
echo "\${jdbc.url}=$database_url?characterEncoding=UTF-8" >> $CONFIG_FILE
echo "\${jdbc.driver}=$database_driverClassName" >> $CONFIG_FILE
echo "\${mail.from}=$mail_from" >> $CONFIG_FILE
echo "\${encryption.enabled}=$ENCR_ENABLED" >> $CONFIG_FILE
echo "\${encryption.algorithm}=$ENCR_ALGORITHM" >> $CONFIG_FILE
echo "\${encryption.salt}=$ENCR_SALT" >> $CONFIG_FILE
echo "\${server.encryption-key}=$SERVER_ENCR_KEY" >> $CONFIG_FILE

cd $FUNAMBOL_HOME/bin
echo "Setting debug mode..." ;
mv funambol-server funambol-server.bak
sed -e s/'-Dfunambol.debug=false'/'-Dfunambol.debug=true'/ funambol-server.bak > funambol-server
rm funambol-server.bak

echo "Setting DS-server logs level to ALL..."
cd ../config/com/funambol/server/logging/logger/
mv funambol.xml funambol.xml.bak
sed -e s/'INFO'/'ALL'/ funambol.xml.bak > funambol.xml
rm funambol.xml.bak


echo "Setting Portal logs level to ALL..."
cd $FUNAMBOL_HOME/config
mv log4j-portal.xml log4j-portal.xml.bak
sed -e s/'INFO'/'ALL'/ log4j-portal.xml.bak > log4j-portal.xml
rm log4j-portal.xml.bak


chmod -Rf +x $FUNAMBOL_HOME

echo
echo "CONFIGURATION execution..."
yes y | $FUNAMBOL_HOME/bin/configure-portal

echo "Setting portal-ext.properties https"
sed -e "s/https/http/g" \
-i $FUNAMBOL_HOME/config/portal/portal-ext.properties
echo "Setting portal-ext.properties https"
sed -e "s/https/http/g" \
-i $FUNAMBOL_HOME/tools/templates/config/portal/portal-ext.properties
echo "Setting Funambol.xml https"
sed -e "s/https/http/g" \
-i $FUNAMBOL_HOME/config/Funambol.xml
echo "Setting Funambol.xml https"
sed -e "s/https/http/g" \
-i $FUNAMBOL_HOME/tools/templates/config/Funambol.xml
echo "Setting portal-ext.properties reload time to 1"
sed -e "s/\(easyconf:reload-delay=60\)\(.*\)\$/easyconf:reload-delay=1/" \
-i $FUNAMBOL_HOME/config/portal/portal-ext.properties
echo "Setting portal-ext.properties templatereload time to 1"
sed -e "s/\(easyconf:reload-delay=60\)\(.*\)\$/easyconf:reload-delay=1/" \
-i $FUNAMBOL_HOME/tools/templates/config/portal/portal-ext.properties
echo "Setting portal-ext.properties allow admin to all"
sed -e "s/sapi.admin.allowedips=.*/sapi.admin.allowedips=*.*.*.*/" \
-i $FUNAMBOL_HOME/config/portal/portal-ext.properties
echo "Setting portal-ext.properties template allow admin to all"
sed -e "s/sapi.admin.allowedips=.*/sapi.admin.allowedips=*.*.*.*/" \
-i $FUNAMBOL_HOME/tools/templates/config/portal/portal-ext.properties
echo "Setting portal-ext.properties allow admin to all"
sed -e "s/sapi.csr.allowedips=.*/sapi.csr.allowedips=*.*.*.*/" \
-i $FUNAMBOL_HOME/config/portal/portal-ext.properties
echo "Setting portal-ext.properties template allow admin to all"
sed -e "s/sapi.csr.allowedips=.*/sapi.csr.allowedips=*.*.*.*/" \
-i $FUNAMBOL_HOME/tools/templates/config/portal/portal-ext.properties
echo "Allow the debug"
sed -e "s/\(sh \.\/catalina\.sh start\)\(.*\)\$/#sh \.\/catalina\.sh start/" \
-i $FUNAMBOL_HOME/bin/funambol-server
echo "Enabling test"
sed -e "s/sapi.validationkey.trusted-user-agent=.*/sapi.validationkey.trusted-user-agent=httpunit\/1.5/" \
-i $FUNAMBOL_HOME/config/portal/portal-ext.properties
sed -e "s/server.encryption-key=.*/server.encryption-key=$SERVER_ENCR_KEY/" \
-i $FUNAMBOL_HOME/config/portal/portal-ext.properties
sed -e "s/encryption.keyfactory.salt=.*/encryption.keyfactory.salt=$ENCR_SALT/" \
-i $FUNAMBOL_HOME/config/portal/portal-ext.properties


echo "Enabling face recognition"
sed -e "s/face-recognition.enable=false/face-recognition.enable=true/" \
-i $FUNAMBOL_HOME/config/portal/portal-ext.properties

#echo "Enabling imagga"
#sed -e "s/computer-vision.key=.*/computer-vision.key=acc_577924f9c44f1ef/" \
#-i $FUNAMBOL_HOME/config/portal/portal-ext.properties
#sed -e "s/computer-vision.secret=.*/computer-vision.secret=a042d5a9f76efb5881b093a13189c099/" \
#-i $FUNAMBOL_HOME/config/portal/portal-ext.properties

echo "Enabling Real networks"
sed -e "s/real-networks.key=.*/real-networks.key=funambol/" \
-i $FUNAMBOL_HOME/config/portal/portal-ext.properties
sed -e "s/real-networks.secret=.*/real-networks.secret=RnVuYW1ib2wtc2VjUmV0LW51bWJFci0xTkU=/" \
-i $FUNAMBOL_HOME/config/portal/portal-ext.properties


sed -e "s/\(#sh \.\/catalina\.sh jpda start\)\(.*\)\$/sh \.\/catalina\.sh jpda start/" \
-i $FUNAMBOL_HOME/bin/funambol-server
echo "Removing secure flag for cookies..."
sed -e "s/<secure>true<\/secure>/<secure>false<\/secure>/" -i $FUNAMBOL_HOME/tools/tomcat/conf/web.xml
sed -e "s/<secure>true<\/secure>/<secure>false<\/secure>/" -i $FUNAMBOL_HOME/tools/tomcat/webapps/ROOT/WEB-INF/web.xml
sed -e "s/<secure>true<\/secure>/<secure>false<\/secure>/" -i $FUNAMBOL_HOME/tools/templates/tools/tomcat/webapps/ROOT/WEB-INF/web.xml
sed -e "s/useHttpOnly=\"true\"/useHttpOnly=\"false\"/" -i $FUNAMBOL_HOME/tools/tomcat/conf/context.xml

cp /tmp/xuggle-xuggler.jar $FUNAMBOL_HOME/tools/tomcat/lib/
cp /tmp/mysql-connector.jar $FUNAMBOL_HOME/tools/tomcat/lib/
echo
echo Finished, move to $FUNAMBOL_HOME and be ready to enjoy!
echo

export JAVA_HOME=$JAVA_HOME
echo Start server
$FUNAMBOL_HOME/bin/funambol start


case $TEST in
   "y")
	echo Initializing AT test
	unzip $ZIP_TEST_FILE -d /opt/
	sed -e "s/server.address=http:\/\/localhost/server.address=http:\/\/$SERVER_IPADDRESS/" \
	-i /opt/acceptance-test/test.properties
	sed -e "s/webservices.url=http:\/\/localhost:8080\/funambol\/services\/admin/webservices.url=http:\/\/$IPADDRESS\/funambol\/services\/admin/" \
	-i /opt/acceptance-test/test.properties
	replace=$(echo $database_url | sed -e 's$/$\\/$g')
	echo $replace
	sed -e "s/portal.database.url=jdbc:mysql:\/\/localhost\/funambol110?characterEncoding=UTF-8/portal.database.url=$replace?characterEncoding=UTF-8/" \
	-i /opt/acceptance-test/test.properties
	sed -e "s/portal.database.username=funambol/portal.database.username=$database_username/" \
	-i /opt/acceptance-test/test.properties
	sed -e "s/portal.database.password=funambol/portal.database.password=$database_password/" \
	-i /opt/acceptance-test/test.properties
	replace=$(echo $FUNAMBOL_HOME | sed -e 's$/$\\/$g')
	echo $replace
	sed -e "s/config.path=\/opt\/onemediahub\/config\//config.path=$replace\/config\//" \
	-i /opt/acceptance-test/test.properties
	case $PRODUCT in
		1)
		;;
		2)
			echo Remove tests not compatible with one biz hub
			## Using papiuser3, invalid user creation for biz
			sed -i '/com.funambol.portal.api.test.PAPIServletAddUserGenericProfileTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletDeleteUserGenericProfileTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletLoginTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletUserPrePopulationTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletMobileSignupPrePopulation/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletSignUpUserTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletUpdateUserGenericProfileTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletUserGenericProfileChangePasswordPropTrue/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletMobileSignup/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletUpdateUserGenericProfileTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletUserGenericProfileChangePasswordPropFalse/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletMediaFolderTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletMediaAudioTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletMediaLabelingTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletValidateCredentialTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletSubscriptionTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletMediaPictureTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletMediaSetTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletMediaTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletMediaValidationCopyrightedTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletMediaValidationIllicitTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletMediaValidationNotYetValidatedTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletProfileTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletSignUpForcedPlusTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletSignUpUserTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletUpdateUserGenericProfileTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletUserGenericProfileChangePasswordPropFalse/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletUserGenericProfileChangePasswordPropTrue/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletUserPrePopulationTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletValidateUserViaCaptcha/d' /opt/acceptance-test/build.xml
			## for biz ther isn't concept of "family", in particular the email template
			sed -i '/com.funambol.portal.api.test.PAPIServletFamilyCloudTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletFamilyCloudUserTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletProfileGetChangesTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletGetFilteredMediaTest/d' /opt/acceptance-test/build.xml
			## different behaviour to folder management (retrieve owner of folders and files)
			sed -i '/com.funambol.portal.api.test.PAPIServletRestoreTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletDataModificationPermissionsTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.msoffice.PAPIMsOfficeActionTest/d' /opt/acceptance-test/build.xml
			sed -i '/com.funambol.portal.api.test.PAPIServletTimelineTest/d' /opt/acceptance-test/build.xml
			## different roles for biz
			sed -i '/com.funambol.portal.api.test.PAPIServletRolesTest/d' /opt/acceptance-test/build.xml
			# captcha verification is disable for biz, the test can be running if it set the properties to true (nothing change for OMH there is already true)
			sed -i '/com.funambol.portal.api.test.PAPIServletSystemTest/d' /opt/acceptance-test/build.xml
			## partining error for fnbl_user
			sed -i '/com.funambol.portal.api.test.ManageRolesTest/d' /opt/acceptance-test/build.xml
			## different behaviour for retrieve profile data
			sed -i '/com.funambol.portal.api.test.PAPIServletUnlinkDeviceTest/d' /opt/acceptance-test/build.xml
		;;
	esac
	cp /tmp/mysql-connector-java-5.1.37-bin.jar /opt/acceptance-test/lib/

	echo Running AT test
	cd /opt/acceptance-test/
	echo Run tests
	/usr/local/apache-ant-1.7.1/bin/ant run-html
   ;;
esac

exec "$@"
