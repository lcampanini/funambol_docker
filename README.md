# Funambol server docker README

## Pre-requisites
Install docker and docker-compose. You can find instruction to install [here](https://docs.docker.com/compose/install/)

## Tips
* .env file contains environment variable; you check the configuration and change only your personal values
* jdk folder contains all files regarding application container (also ant to run AT tests)
* db folder contains files regarding the schema of database
* docker-compose.yml contains the configuration about the two container (mysql and jdk)

## Steps to follow
* check the configuration on .env file
* as sudo, run install.sh

## Problems
* docker caching the built image, so if you change some configuration or step to execution, you must remeber to remove all built images with command docker "docker rmi $(docker images -aq)" before check the latest changes
* if you would use the script to run your local server, you should copy the tgz file created with the portal build under the path _./.docker_ and renaming _tar.tgz_